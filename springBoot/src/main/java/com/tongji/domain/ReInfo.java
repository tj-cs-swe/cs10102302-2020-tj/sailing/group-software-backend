package com.tongji.domain;

public class ReInfo {
    private boolean status;//登录状态 false为登录失败 true为登录成功
    private String msg;//登录信息记录

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "LoginInfo{" +
                "status=" + status +
                ", msg='" + msg + '\'' +
                '}';
    }
}
