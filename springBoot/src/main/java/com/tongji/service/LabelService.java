package com.tongji.service;

import com.tongji.domain.Label;

import java.util.List;

public interface LabelService {//标签接口

    public List<Label> findAll();


    public List<Label> findTeamLabel(String teamId);//查找队伍相关标签


    public List<Label> findOther(String teamId);//查找不属于队伍的标签
}
