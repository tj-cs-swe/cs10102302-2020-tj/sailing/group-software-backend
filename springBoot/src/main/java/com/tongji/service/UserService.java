package com.tongji.service;
import com.tongji.domain.LoginInfo;
import com.tongji.domain.RegisterInfo;
import com.tongji.domain.Team;
import com.tongji.domain.User;

import java.util.List;


public interface UserService {

    //登录处理
    public User findByUsername(String username);

    //查找个人信息
    public User findById(String id);


    //注册处理
    public RegisterInfo register(User user);

    //更新个人信息
    public void updateInfo(User user);



    //**********************************************************

    //查找队伍申请者
    public List<User>findApplyUser(String teamid);

    //查找加入队伍
    public List<User> findJoinUser(String teamid);

    //查找队伍创建者
    public User  findCreateUser(String teamid);

    //查找申请退出
    public List<User>findDropUser(String teamid);
}
