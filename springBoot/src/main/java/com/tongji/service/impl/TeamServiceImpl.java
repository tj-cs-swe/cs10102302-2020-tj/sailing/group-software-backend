package com.tongji.service.impl;

import com.github.pagehelper.PageHelper;
import com.tongji.dao.LogsDao;
import com.tongji.dao.TeamDao;
import com.tongji.dao.UserDao;
import com.tongji.domain.Logs;
import com.tongji.domain.ReInfo;
import com.tongji.domain.Team;
import com.tongji.domain.User;
import com.tongji.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    TeamDao teamDao;

    @Autowired
    LogsDao logsDao;


    @Autowired
    UserDao userDao;

    /**
     * 根据传入的队伍信息进行创建队伍的操作：
     * 1.先向数据库中查找队伍名是否存在
     * 2.存在直接设置Reinfo为:队伍名存在,创建失败
     * 3.不存在直接调用dao层的方法进行数据插入，并设置Reinfo
     * @param team 队伍信息
     * @return Reinfo反馈信息封装类
     */
    @Override
    public ReInfo createTeam(Team team, List<String>ids) {
        ReInfo info=new ReInfo();

        Team byTeamName = teamDao.findByTeamName(team.getTeamname());
        if(byTeamName==null){
            String teamid=UUID.randomUUID().toString();
            team.setId(teamid);
            team.setNumber(1);//队伍初始人数为1
            team.setStatus(0);//初始状态未组队
            teamDao.addTeam(team);
            info.setMsg("队伍创建成功");
            info.setStatus(true);

            //为队伍添加标签
            if(ids!=null){
                for (String id : ids) {
                    teamDao.addLabel(teamid,id);
                }
            }

        }else {
            info.setMsg("队伍名重复，创建失败");
            info.setStatus(false);
        }

        return info;
    }


    /**
     * 直接调用dao层方法进行队伍信息更新
     * @param team
     */
    @Override
    public void updateTeam(Team team,List<String>ids) {

        teamDao.deleteAllLabel(team.getId());//先删除该队伍的所有标签
        teamDao.updateById(team);
        for (String id : ids) {
            teamDao.addLabel(team.getId(),id);//再逐一添加标签
        }
    }

    /**
     * 直接调用dao层方法进行队伍详情查询
     * @param teamId 队伍 id
     * @return 查询结果
     */
    @Override
    public Team findById(String teamId) {
        return teamDao.findByTeamId(teamId);
    }

    /**
     * 分页查询所有的队伍
     *
     * @return 队伍列表
     */
    @Override
    public List<Team> findAll() {

        List<Team> all = teamDao.findAll();
        return all;
    }

    /**
     * 根据关键词查找队伍
     * @param
     * @return
     */
    @Override
    public List<Team> findByWord(String word) {

        return teamDao.findByWord(word);
    }


    /**
     * 根据标签筛选队伍
     *
     * @param label_ids 标签id集合
     * @return
     */
    @Override
    public List<Team> findByLabel(List<String> label_ids) {
        Set<String>teamids=new HashSet<>();

        //求出具有所有标签的队伍id
        for (String label_id : label_ids) {

            if(teamids.isEmpty()){
                teamids.addAll(teamDao.findByLabel(label_id));
            }else{
                teamids.retainAll(teamDao.findByLabel(label_id));
            }

        }

        List<Team> byIds=new ArrayList<Team>();

        for (String teamid : teamids) {

            byIds.add(teamDao.findByTeamId(teamid));
        }



        return byIds;
    }


    //**************************************************************************************


    /**
     * 查询申请的队伍信息
     * @param uid
     * @return
     */
    @Override
    public List<Team> findApplyTeam(String uid) {
        List<String> applyTeamId = teamDao.findApplyTeamId(uid);

        List<Team> team=new ArrayList<>();
        for (String id : applyTeamId) {
            team.add(teamDao.findByTeamId(id));
        }
        return team;
    }

    /**
     * 查询加入的队伍信息
     * @param uid
     * @return
     */
    @Override
    public List<Team> findJoinTeam(String uid) {
        List<String> joinTeamId = teamDao.findJoinTeamId(uid);
        List<Team> team=new ArrayList<>();
        for (String id : joinTeamId) {
            team.add(teamDao.findByTeamId(id));
        }
        return team;
    }

    /**
     * 查询创建的队伍信息
     * @param uid
     * @return
     */
    @Override
    public List<Team> findCreateTeam(String uid) {
        return teamDao.findCreateTeam(uid);
    }


    //申请加入队伍
    @Override
    public void applyTeam(String uid, String teamid) {
        teamDao.addApplyUser(uid,teamid);
    }

    //拒绝加入
    @Override
    public void rejectApply(String uid, String teamid) {
        teamDao.deleteApplyUser(uid,teamid);

        //添加拒绝信息
        Logs logs=new Logs();
        logs.setId(UUID.randomUUID().toString());
        logs.setStatus(0);
        logs.setUid(uid);
        logs.setLtime(new Date());
        Team team = teamDao.findByTeamId(teamid);
        logs.setMsg("非常抱歉,你加入队伍<"+team.getTeamname()+">的申请已经被拒绝");
        logsDao.addLog(logs);

    }

    //同意加入
    @Override
    public void joinTeam(String uid, String teamid) {
        teamDao.deleteApplyUser(uid,teamid);
        teamDao.addJoinUser(uid,teamid);
        Team team = teamDao.findByTeamId(teamid);
        teamDao.updateMember(teamid,team.getNumber()+1);

        Logs logs=new Logs();
        logs.setId(UUID.randomUUID().toString());
        logs.setStatus(0);
        logs.setUid(uid);
        logs.setLtime(new Date());
        logs.setMsg("恭喜你,你加入队伍<"+team.getTeamname()+">的申请已经被接受，你已经成功加入该队伍");
        logsDao.addLog(logs);
    }

    @Override
    public void quitApply(String uid, String teamid) {
        teamDao.deleteApplyUser(uid,teamid);
    }

    //申请退出
    @Override
    public void applyDropTeam(String uid, String teamid) {
        teamDao.adddropUser(uid, teamid);
    }

    //拒绝退出
    @Override
    public void rejectDrop(String uid, String teamid) {
        teamDao.deleteDropUser(uid, teamid);
        //添加拒绝信息
        Logs logs=new Logs();
        logs.setId(UUID.randomUUID().toString());
        logs.setStatus(0);
        logs.setUid(uid);
        logs.setLtime(new Date());
        Team team = teamDao.findByTeamId(teamid);
        logs.setMsg("非常抱歉,你退出队伍<"+team.getTeamname()+">的申请已经被拒绝");
        logsDao.addLog(logs);

    }

    //同意退出
    @Override
    public void dropTeam(String uid, String teamid) {
        teamDao.deleteDropUser(uid, teamid);
        teamDao.deleteJoinUser(uid, teamid);

        Team team = teamDao.findByTeamId(teamid);
        teamDao.updateMember(teamid,team.getNumber()-1);
        Logs logs=new Logs();
        logs.setId(UUID.randomUUID().toString());
        logs.setStatus(0);
        logs.setUid(uid);
        logs.setLtime(new Date());
        logs.setMsg("你退出队伍<"+team.getTeamname()+">的申请已经被接受，你已经成功退出该队伍");
        logsDao.addLog(logs);


        //-------------------------------------通知其他成员
        List<User> joinUser = userDao.findJoinUser(teamid);
        User byId = userDao.findById(uid);
        for (User user : joinUser) {
            Logs log=new Logs();
            logs.setId(UUID.randomUUID().toString());
            logs.setStatus(0);
            logs.setUid(user.getId());
            logs.setLtime(new Date());
            logs.setMsg("成员"+byId.getNickname()+"已经退出队伍<"+team.getTeamname()+">");
            logsDao.addLog(logs);
        }


    }

    @Override
    public void quitDrop(String uid, String teamid) {
        teamDao.deleteDropUser(uid,teamid);
    }

    //完成组队
    @Override
    public ReInfo completeTeam(String teamid) {

        Team team = teamDao.findByTeamId(teamid);
        ReInfo reInfo=new ReInfo();
        if(team.getNumber()>=2){
            reInfo.setStatus(true);
            reInfo.setMsg("完成组队成功!");
            teamDao.complete(teamid);
            //通知成员
            List<User> joinUser = userDao.findJoinUser(teamid);
            for (User user : joinUser) {
                Logs logs=new Logs();
                logs.setId(UUID.randomUUID().toString());
                logs.setStatus(0);
                logs.setUid(user.getId());
                logs.setLtime(new Date());
                logs.setMsg("你加入的队伍<"+team.getTeamname()+">已经完成组队");
                logsDao.addLog(logs);
            }

        }else{
            reInfo.setStatus(false);
            reInfo.setMsg("人数不足，无法完成组队");
        }
        return reInfo;
    }

    //解散队伍
    @Override
    public void breakTeam(String teamid) {
        Team team = teamDao.findByTeamId(teamid);

        //通知成员
        List<User> joinUser = userDao.findJoinUser(teamid);
        for (User user : joinUser) {
            Logs logs=new Logs();
            logs.setId(UUID.randomUUID().toString());
            logs.setStatus(0);
            logs.setUid(user.getId());
            logs.setLtime(new Date());
            logs.setMsg("你加入的队伍<"+team.getTeamname()+">已经解散");
            logsDao.addLog(logs);
        }


        teamDao.deleteAllMember(teamid);
        teamDao.deleteAllLabel(teamid);
        teamDao.deleteAllDropApply(teamid);
        teamDao.deleteAllJoinApply(teamid);
        teamDao.deleteTeam(teamid);

    }




}
