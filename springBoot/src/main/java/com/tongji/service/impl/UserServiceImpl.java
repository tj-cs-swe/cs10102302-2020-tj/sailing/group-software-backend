package com.tongji.service.impl;

import com.tongji.dao.UserDao;
import com.tongji.domain.LoginInfo;
import com.tongji.domain.RegisterInfo;
import com.tongji.domain.User;
import com.tongji.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;


    @Override
    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }

    @Override
    public User findById(String id) {
        return userDao.findById(id);
    }

    /**
     * 用户注册的业务处理：
     * 用户名存在则注册失败
     * 用户名不存在则调用dao层方法将用户数据插入到数据库中
     * @param user 用户信息封装类
     * @return 注册信息
     */
    @Override
    public RegisterInfo register(User user) {
        User byUsername = userDao.findByUsername(user.getUsername());
        RegisterInfo info=new RegisterInfo();
        if(byUsername==null){
            userDao.addUser(user);
            info.setMsg("注册成功");
            info.setStatus(true);
            return info;
        }else {
            info.setMsg("用户名存在注册失败");
            info.setStatus(false);
            return info;
        }

    }


    /**
     * 更新个人信息：
     * 直接调用userdao的update方法即可
     * 不需要进行额外的信息
     * @param user 需要更新的个人信息
     */
    @Override
    public void updateInfo(User user) {
        userDao.updateById(user);
    }


    /**
     * 查找申请者
     * @param teamid
     * @return
     */
    @Override
    public List<User> findApplyUser(String teamid) {
        return userDao.findApplyUser(teamid);
    }

    //查找加入者
    @Override
    public List<User> findJoinUser(String teamid) {
        return userDao.findJoinUser(teamid);
    }

    /**
     * 查找创建者
     * @param teamid
     * @return
     */
    @Override
    public User findCreateUser(String teamid) {
        return userDao.findCreateUser(teamid);
    }

    @Override
    public List<User> findDropUser(String teamid) {
        return userDao.findDropUser(teamid);
    }


}
