package com.tongji.service;

import com.tongji.domain.Logs;

import java.util.List;

public interface LogsService {
    public List<Logs> findUnread(String uid);
    public List<Logs> findread(String uid);
    public void confirmLog(String id);
}
