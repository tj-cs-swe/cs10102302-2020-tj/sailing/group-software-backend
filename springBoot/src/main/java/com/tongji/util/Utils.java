package com.tongji.util;

import com.tongji.domain.User;

import javax.servlet.http.Cookie;
import java.util.Map;

public class Utils {

    public static User getUser(Map<String,String> parm){
        User user=new User();
        for (Map.Entry<String, String> entry : parm.entrySet()) {
            if(entry.getKey().equals("username")){
                user.setUsername(entry.getValue());
            }
            if(entry.getKey().equals("password")){
                user.setPassword(entry.getValue());
            }
            if(entry.getKey().equals("nickname")){
                user.setNickname(entry.getValue());
            }
            if(entry.getKey().equals("sex")){
                user.setSex(entry.getValue());
            }
            if(entry.getKey().equals("phonenum")){
                user.setPhonenum(entry.getValue());
            }
            if(entry.getKey().equals("email")){
                user.setEmail(entry.getValue());
            }

        }
        return user;
    }

    public static String getUid(Cookie []cookies){
        String value="";
        //查找uid
        for (Cookie cookie : cookies) {
            if(cookie.getName().equals("userId")){
                value=cookie.getValue();
                break;
            }
        }
        return value;
    }

}
