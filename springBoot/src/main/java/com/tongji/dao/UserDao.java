package com.tongji.dao;


import com.tongji.domain.Team;
import com.tongji.domain.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao {

    /**
     * 通过用户名去数据库查找用户
     * @param username 用户名
     * @return 返回查找的用户
     */
    @Select("select*from user where username=#{username}")
    public User findByUsername(String username);


    @Select("select*from user where id=#{id}")
    public User findById(String id);


    /**
     * 向表中添加用户信息
     *
     * @param user 用户
     */
    @Insert("insert into user values(#{user.id},#{user.username},#{user.password},#{user.nickname},#{user.sex},#{user.phonenum},#{user.email})")
    public void addUser(@Param("user") User user);


    /**
     * 根据用户的ID锁定用户,对其信息进行修改update
     * @param user
     */
    @Update("update user set nickname=#{user.nickname},sex=#{user.sex},phonenum=#{user.phonenum},email=#{user.email} where id=#{user.id}")
    public void updateById(@Param("user")User user);




    //********************************************************************************************

    /**
     * 查找申请人员信息
     * @param teamid
     * @return
     */
    @Select("select*from user where id in(select user_id from user_tb_team where team_id=#{id})")
    public List<User>findApplyUser(@Param("id") String teamid);


    /**
     * 查找队伍创建人的信息
     * @param teamid
     * @return
     */
    @Select("select*from user where id=(select uid from team where id=#{id})")
    public User findCreateUser(@Param("id") String teamid);



    @Select("select*from user where id in(select user_id from user_team where team_id=#{id})")
    public List<User> findJoinUser(@Param("id") String teamid);


    @Select("select*from user where id in(select user_id from user_drop_team where team_id=#{id})")
    public List<User>findDropUser(@Param("id") String teamid);

}
