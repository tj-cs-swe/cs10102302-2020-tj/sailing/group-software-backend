package com.tongji.dao;

import com.tongji.domain.Logs;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogsDao {//通知信息类

    //添加信息
    @Insert("insert into logs values(#{logs.id},#{logs.uid},#{logs.msg},#{logs.ltime},#{logs.status})")
    public void addLog(@Param("logs") Logs logs);

    //查找已读信息
    @Select("select*from logs where uid=#{uid} and status=#{status}")
    public List<Logs> findUnread(@Param("uid") String uid,@Param("status") int status);

    //查找未读信息
    @Select("select*from logs where uid=#{uid} and status=#{status} ")
    public List<Logs> findread(@Param("uid") String uid,@Param("status")int status);

    //确认信息
    @Update("update logs set status=#{status} where id=#{id}")
    public void confirmLog(@Param("id") String id,@Param("status")int status);
}
