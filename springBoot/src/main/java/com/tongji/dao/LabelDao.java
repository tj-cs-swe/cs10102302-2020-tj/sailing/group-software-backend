package com.tongji.dao;

import com.tongji.domain.Label;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

//标签操作
@Repository
public interface LabelDao {

    /**
     * 查询所有标签
     */
    @Select("select*from label")
    public List<Label> findAll();


    /**
     * 通过teamid查找其相关联的标签
     * @param teamId
     * @return
     */
    @Select("select*from label where id in(select label_id from team_label where team_id=#{teamId})")
    public List<Label>findByTeamId(String teamId);


    /**
     * 查找不属于该队伍的所有标签
     * @param teamId 队伍id
     * @return
     */
    @Select("select*from label where id not in(select label_id from team_label where team_id=#{teamId})")
    public List<Label>findOtherLabel(String teamId);
}
